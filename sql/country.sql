--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4
-- Dumped by pg_dump version 12.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: locus_country; Type: TABLE DATA; Schema: public; Owner: argus
--

COPY public.locus_country (id, name_std, name, code, code3, population, area, currency, currency_name, currency_symbol, language_codes, phone, tld, postal_code_format, postal_code_regex, capital, created, last_updated, slug, continent_id, wikidata) FROM stdin;
933860	Botswana	Botswana	BW	BWA	2029307	600370	BWP	Pula	P	en-BW,tn-BW	267	bw			Gaborone	2018-12-03 23:28:50.416417-05	2020-09-13 16:22:39.439602-04	botswana	6255146	Q963
290557	United Arab Emirates	United Arab Emirates	AE	ARE	4975593	82880	AED	Dirham	د.إ	ar-AE,fa,en,hi,ur	971	ae			Abu Dhabi	2018-12-03 23:28:50.330576-05	2020-09-13 15:59:35.896779-04	united-arab-emirates	6255147	Q878
2260494	Republic of the Congo	Republic of the Congo	CG	COG	3039126	342000	XAF	Franc	Fr	fr-CG,kg,ln-CG	242	cg			Brazzaville	2018-12-03 23:28:50.434455-05	2020-09-13 15:55:35.228205-04	republic-of-the-congo	6255146	Q971
3865483	Argentina	Argentina	AR	ARG	41343201	2766890	ARS	Peso	$	es-AR,en,it,de,fr,gn	54	ar	@####@@@	^[A-Z]?\\d{4}[A-Z]{0,3}$	Buenos Aires	2018-12-03 23:28:50.352319-05	2020-09-13 16:05:13.114673-04	argentina	6255150	Q414
2233387	Cameroon	Cameroon	CM	CMR	19294149	475440	XAF	Franc	Fr	en-CM,fr-CM	237	cm			Yaounde	2018-12-03 23:28:50.448105-05	2020-09-13 15:55:25.448328-04	cameroon	6255146	Q1009
3895114	Chile	Chile	CL	CHL	16746491	756950	CLP	Peso	$	es-CL	56	cl	#######	^(\\d{7})$	Santiago	2018-12-03 23:28:50.445149-05	2020-09-13 16:05:18.253692-04	chile	6255150	Q298
1252634	Bhutan	Bhutan	BT	BTN	699847	47000	BTN	Ngultrum	Nu	dz	975	bt			Thimphu	2018-12-03 23:28:50.411523-05	2020-09-13 15:49:54.660701-04	bhutan	6255147	Q917
433561	Burundi	Burundi	BI	BDI	9863117	27830	BIF	Franc	Fr	fr-BI,rn	257	bi			Bujumbura	2018-12-03 23:28:50.388762-05	2020-09-13 16:06:34.258166-04	burundi	6255146	Q967
3573345	Bermuda	Bermuda	BM	BMU	65365	53	BMD	Dollar	$	en-BM,pt	+1-441	bm	@@ ##	^([A-Z]{2}\\d{2})$	Hamilton	2018-12-03 23:28:50.396236-05	2020-09-13 16:04:24.338623-04	bermuda	6255149	Q23635
2658434	Switzerland	Switzerland	CH	CHE	7581000	41290	CHF	Franc	Fr	de-CH,fr-CH,it-CH,rm	41	ch	####	^(\\d{4})$	Bern	2018-12-03 23:28:50.43694-05	2020-09-13 15:57:55.878445-04	switzerland	6255148	Q39
2453866	Mali	Mali	ML	MLI	13796354	1240000	XOF	Franc	Fr	fr-ML,bm	223	ml			Bamako	2018-12-03 23:28:50.713215-05	2020-09-13 15:56:22.110576-04	mali	6255146	Q912
1547376	Cocos Islands	Cocos Islands	CC	CCK	628	14	AUD	Dollar	$	ms-CC,en	61	cc			West Island	2018-12-03 23:28:50.426654-05	2020-09-13 15:51:44.687294-04	cocos-islands	6255147	Q36004
1899402	Cook Islands	Cook Islands	CK	COK	21388	240	NZD	Dollar	$	en-CK,mi	682	ck			Avarua	2018-12-03 23:28:50.442528-05	2020-09-13 15:53:36.660319-04	cook-islands	6255151	Q26988
2395170	Benin	Benin	BJ	BEN	9056010	112620	XOF	Franc	Fr	fr-BJ	229	bj			Porto-Novo	2018-12-03 23:28:50.391214-05	2020-09-13 15:56:04.955361-04	benin	6255146	Q962
239880	Central African Republic	Central African Republic	CF	CAF	4844927	622984	XAF	Franc	Fr	fr-CF,sg,ln,kg	236	cf			Bangui	2018-12-03 23:28:50.431899-05	2020-09-13 15:56:06.961282-04	central-african-republic	6255146	Q929
587116	Azerbaijan	Azerbaijan	AZ	AZE	8303512	86600	AZN	Manat	m	az,ru,hy	994	az	AZ ####	^(?:AZ)*(\\d{4})$	Baku	2018-12-03 23:28:50.368078-05	2020-09-13 16:10:43.982212-04	azerbaijan	6255147	Q227
2287781	Ivory Coast	Ivory Coast	CI	CIV	21058798	322460	XOF	Franc	Fr	fr-CI	225	ci			Yamoussoukro	2018-12-03 23:28:50.439624-05	2020-09-13 15:55:41.264406-04	ivory-coast	6255146	Q1008
1210997	Bangladesh	Bangladesh	BD	BGD	156118464	144000	BDT	Taka	৳	bn-BD,en	880	bd	####	^(\\d{4})$	Dhaka	2018-12-03 23:28:50.376234-05	2020-09-13 15:49:44.210258-04	bangladesh	6255147	Q902
3572887	Bahamas	Bahamas	BS	BHS	301790	13940	BSD	Dollar	$	en-BS	+1-242	bs			Nassau	2018-12-03 23:28:50.409122-05	2020-09-13 16:04:24.249683-04	bahamas	6255149	Q778
630336	Belarus	Belarus	BY	BLR	9685000	207600	BYN	Belarusian ruble	\N	be,ru	375	by	######	^(\\d{6})$	Minsk	2018-12-03 23:28:50.418838-05	2020-09-13 16:12:01.154274-04	belarus	6255148	Q184
3469034	Brazil	Brazil	BR	BRA	201103330	8511965	BRL	Real	R$	pt-BR,es,en,fr	55	br	#####-###	^\\d{5}-\\d{3}$	Brasilia	2018-12-03 23:28:50.406551-05	2020-09-13 16:04:02.708941-04	brazil	6255150	Q155
2802361	Belgium	Belgium	BE	BEL	10403000	30510	EUR	Euro	€	nl-BE,fr-BE,de-BE	32	be	####	^(\\d{4})$	Brussels	2018-12-03 23:28:50.378774-05	2020-09-13 15:58:56.049471-04	belgium	6255148	Q31
7626844	Bonaire, Saint Eustatius and Saba 	Bonaire, Saint Eustatius and Saba 	BQ	BES	18012	328	USD	Dollar	$	nl,pap,en	599	bq				2018-12-03 23:28:50.403945-05	2020-09-13 16:19:08.328512-04	bonaire-saint-eustatius-and-saba	6255149	Q27561
2361809	Burkina Faso	Burkina Faso	BF	BFA	16241811	274200	XOF	Franc	Fr	fr-BF,mos	226	bf			Ouagadougou	2018-12-03 23:28:50.381183-05	2020-09-13 15:55:53.41035-04	burkina-faso	6255146	Q965
2782113	Austria	Austria	AT	AUT	8205000	83858	EUR	Euro	€	de-AT,hr,hu,sl	43	at	####	^(\\d{4})$	Vienna	2018-12-03 23:28:50.357852-05	2020-09-13 15:58:50.226901-04	austria	6255148	Q40
3923057	Bolivia	Bolivia	BO	BOL	9947418	1098580	BOB	Boliviano	Bs.	es-BO,qu,ay	591	bo			Sucre	2018-12-03 23:28:50.401525-05	2020-09-13 16:05:22.226961-04	bolivia	6255150	Q750
3041565	Andorra	Andorra	AD	AND	84000	468	EUR	Euro	€	ca	376	ad	AD###	^(?:AD)*(\\d{3})$	Andorra la Vella	2018-12-03 23:28:50.324383-05	2020-09-13 16:00:44.885669-04	andorra	6255148	Q228
174982	Armenia	Armenia	AM	ARM	2968000	29800	AMD	Dram	դր.	hy	374	am	######	^(\\d{6})$	Yerevan	2018-12-03 23:28:50.344421-05	2020-09-13 15:52:49.45452-04	armenia	6255147	Q399
1820814	Brunei	Brunei	BN	BRN	395027	5770	BND	Dollar	$	ms-BN,en-BN	673	bn	@@####	^([A-Z]{2}\\d{4})$	Bandar Seri Begawan	2018-12-03 23:28:50.398837-05	2020-09-13 15:53:12.72958-04	brunei	6255147	Q921
2077456	Australia	Australia	AU	AUS	21515754	7686850	AUD	Dollar	$	en-AU	61	au	####	^(\\d{4})$	Canberra	2018-12-03 23:28:50.360434-05	2020-09-13 15:54:24.903279-04	australia	6255151	Q408
290291	Bahrain	Bahrain	BH	BHR	738004	665	BHD	Dinar	ب.د	ar-BH,en,fa,ur	973	bh	####|###	^(\\d{3}\\d?)$	Manama	2018-12-03 23:28:50.386025-05	2020-09-13 15:59:34.778247-04	bahrain	6255147	Q398
3277605	Bosnia and Herzegovina	Bosnia and Herzegovina	BA	BIH	4590000	51129	BAM	Marka	KM	bs,hr-BA,sr-BA	387	ba	#####	^(\\d{5})$	Sarajevo	2018-12-03 23:28:50.37081-05	2020-09-13 16:03:20.268328-04	bosnia-and-herzegovina	6255148	Q225
3351879	Angola	Angola	AO	AGO	13068161	1246700	AOA	Kwanza	Kz	pt-AO	244	ao			Luanda	2018-12-03 23:28:50.347196-05	2020-09-13 16:03:29.432494-04	angola	6255146	Q916
3371123	Bouvet Island	Bouvet Island	BV	BVT	0	49	NOK	Krone	kr			bv				2018-12-03 23:28:50.413948-05	2020-09-13 16:03:31.557389-04	bouvet-island	6255152	Q23408
3374084	Barbados	Barbados	BB	BRB	285653	431	BBD	Dollar	$	en-BB	+1-246	bb	BB#####	^(?:BB)*(\\d{5})$	Bridgetown	2018-12-03 23:28:50.373615-05	2020-09-13 16:03:32.279855-04	barbados	6255149	Q244
3573511	Anguilla	Anguilla	AI	AIA	13254	102	XCD	Dollar	$	en-AI	+1-264	ai			The Valley	2018-12-03 23:28:50.338254-05	2020-09-13 16:04:24.356929-04	anguilla	6255149	Q25228
3577279	Aruba	Aruba	AW	ABW	71566	193	AWG	Guilder	ƒ	nl-AW,pap,es,en	297	aw			Oranjestad	2018-12-03 23:28:50.362995-05	2020-09-13 16:04:24.901299-04	aruba	6255149	Q21203
3578476	Saint Barthelemy	Saint Barthelemy	BL	BLM	8450	21	EUR	Euro	€	fr	590	gp	#####	^(\\d{5})$	Gustavia	2018-12-03 23:28:50.393669-05	2020-09-13 16:04:25.104603-04	saint-barthelemy	6255149	Q25362
3582678	Belize	Belize	BZ	BLZ	314522	22966	BZD	Dollar	$	en-BZ,es	501	bz			Belmopan	2018-12-03 23:28:50.42142-05	2020-09-13 16:04:26.024181-04	belize	6255149	Q242
3686110	Colombia	Colombia	CO	COL	47790000	1138910	COP	Peso	$	es-CO	57	co	######	^(\\d{6})$	Bogota	2018-12-03 23:28:50.453517-05	2020-09-13 16:04:46.833201-04	colombia	6255150	Q739
5880801	American Samoa	American Samoa	AS	ASM	57881	199	USD	Dollar	$	en-AS,sm,to	+1-684	as	#####-####	96799	Pago Pago	2018-12-03 23:28:50.354906-05	2020-09-13 16:10:46.074137-04	american-samoa	6255151	Q16641
6251999	Canada	Canada	CA	CAN	33679000	9984670	CAD	Dollar	$	en-CA,fr-CA,iu	1	ca	@#@ #@#	^([ABCEGHJKLMNPRSTVXY]\\d[ABCEGHJKLMNPRSTVWXYZ]) ?(\\d[ABCEGHJKLMNPRSTVWXYZ]\\d)$ 	Ottawa	2018-12-03 23:28:50.424086-05	2020-09-13 16:11:56.088085-04	canada	6255149	Q16
661882	Aland Islands	Aland Islands	AX	ALA	26711	1580	EUR	Euro	€	sv-AX	+358-18	ax	#####	^(?:FI)*(\\d{5})$	Mariehamn	2018-12-03 23:28:50.365542-05	2020-09-13 16:15:57.473558-04	aland-islands	6255148	Q5689
6697173	Antarctica	Antarctica	AQ	ATA	0	14000000			\N			aq				2018-12-03 23:28:50.34979-05	2020-09-13 16:16:19.326052-04	antarctica	6255152	Q21590062
732800	Bulgaria	Bulgaria	BG	BGR	7148785	110910	BGN	Lev	лв	bg,tr-BG,rom	359	bg	####	^(\\d{4})$	Sofia	2018-12-03 23:28:50.383615-05	2020-09-13 16:18:05.048835-04	bulgaria	6255148	Q219
783754	Albania	Albania	AL	ALB	2986952	28748	ALL	Lek	L	sq,el	355	al	####	^(\\d{4})$	Tirana	2018-12-03 23:28:50.341292-05	2020-09-13 16:20:00.039696-04	albania	6255148	Q222
223816	Djibouti	Djibouti	DJ	DJI	740528	23000	DJF	Franc	Fr	fr-DJ,ar,so-DJ,aa	253	dj			Djibouti	2018-12-03 23:28:50.478809-05	2020-09-13 15:55:27.459218-04	djibouti	6255146	Q977
2461445	Western Sahara	Western Sahara	EH	ESH	273008	266000	MAD	Dirham	د.م.	ar,mey	212	eh			El-Aaiun	2018-12-03 23:28:50.498977-05	2020-09-13 22:20:29.009255-04	western-sahara	6255146	Q6250
3658394	Ecuador	Ecuador	EC	ECU	14790608	283560	USD	Dollar	$	es-EC	593	ec	@####@	^([a-zA-Z]\\d{4}[a-zA-Z])$	Quito	2018-12-03 23:28:50.491601-05	2020-09-13 16:04:40.20853-04	ecuador	6255150	Q736
2510769	Spain	Spain	ES	ESP	46505963	504782	EUR	Euro	€	es-ES,ca,gl,eu,oc	34	es	#####	^(\\d{5})$	Madrid	2018-12-03 23:28:50.504261-05	2020-09-13 15:56:48.072248-04	spain	6255148	Q29
2400553	Gabon	Gabon	GA	GAB	1545255	267667	XAF	Franc	Fr	fr-GA	241	ga			Libreville	2018-12-03 23:28:50.525428-05	2020-09-13 15:56:07.696465-04	gabon	6255146	Q1000
3378535	Guyana	Guyana	GY	GUY	748486	214970	GYD	Dollar	$	en-GY	592	gy			Georgetown	2018-12-03 23:28:50.573032-05	2020-09-13 16:03:33.141266-04	guyana	6255150	Q734
2622320	Faroe Islands	Faroe Islands	FO	FRO	48228	1399	DKK	Krone	kr	fo,da-FO	298	fo	###	^(?:FO)*(\\d{3})$	Torshavn	2018-12-03 23:28:50.520101-05	2020-09-13 15:57:33.6836-04	faroe-islands	6255148	Q4628
3425505	Greenland	Greenland	GL	GRL	56375	2166086	DKK	Krone	kr	kl,da-GL,en	299	gl	####	^(\\d{4})$	Nuuk	2018-12-03 23:28:50.547102-05	2020-09-13 16:03:43.125553-04	greenland	6255149	Q223
2205218	Fiji	Fiji	FJ	FJI	875983	18270	FJD	Dollar	$	en-FJ,fj	679	fj			Suva	2018-12-03 23:28:50.512387-05	2020-09-13 15:55:12.743732-04	fiji	6255151	Q712
146669	Cyprus	Cyprus	CY	CYP	1102677	9250	EUR	Euro	€	el-CY,tr-CY,en	357	cy	####	^(\\d{4})$	Nicosia	2018-12-03 23:28:50.471021-05	2020-09-13 15:51:12.656452-04	cyprus	6255148	Q229
338010	Eritrea	Eritrea	ER	ERI	5792984	121320	ERN	Nakfa	Nfk	aa-ER,ar,tig,kun,ti-ER	291	er			Asmara	2018-12-03 23:28:50.501503-05	2020-09-13 16:03:33.426227-04	eritrea	6255146	Q986
390903	Greece	Greece	GR	GRC	11000000	131940	EUR	Euro	€	el-GR,en,fr	30	gr	### ##	^(\\d{5})$	Athens	2018-12-03 23:28:50.560215-05	2020-09-13 16:05:20.378931-04	greece	6255148	Q41
3595528	Guatemala	Guatemala	GT	GTM	13550440	108890	GTQ	Quetzal	Q	es-GT	502	gt	#####	^(\\d{5})$	Guatemala City	2018-12-03 23:28:50.565399-05	2020-09-13 16:04:30.429743-04	guatemala	6255149	Q774
3608932	Honduras	Honduras	HN	HND	7989415	112090	HNL	Lempira	L	es-HN,cab,miq	504	hn	@@####	^([A-Z]{2}\\d{4})$	Tegucigalpa	2018-12-03 23:28:50.581013-05	2020-09-13 16:04:32.337529-04	honduras	6255149	Q783
3624060	Costa Rica	Costa Rica	CR	CRI	4516220	51100	CRC	Colon	₡	es-CR,en	506	cr	#####	^(\\d{5})$	San Jose	2018-12-03 23:28:50.457533-05	2020-09-13 16:04:34.950733-04	costa-rica	6255149	Q800
3579143	Guadeloupe	Guadeloupe	GP	GLP	443000	1780	EUR	Euro	€	fr-GP	590	gp	#####	^((97|98)\\d{3})$	Basse-Terre	2018-12-03 23:28:50.554873-05	2020-09-13 16:04:25.18178-04	guadeloupe	6255149	Q17012
3562981	Cuba	Cuba	CU	CUB	11423000	110860	CUP	Peso	$	es-CU,pap	53	cu	CP #####	^(?:CP)*(\\d{5})$	Havana	2018-12-03 23:28:50.460781-05	2020-09-13 16:04:22.740229-04	cuba	6255149	Q241
3017382	France	France	FR	FRA	64768389	547030	EUR	Euro	€	fr-FR,frp,br,co,ca,eu,oc	33	fr	#####	^(\\d{5})$	Paris	2018-12-03 23:28:50.522661-05	2020-09-13 16:00:30.195943-04	france	6255148	Q142
2300660	Ghana	Ghana	GH	GHA	24339838	239460	GHS	Cedi	₵	en-GH,ak,ee,tw	233	gh			Accra	2018-12-03 23:28:50.541497-05	2020-09-13 15:55:44.105583-04	ghana	6255146	Q117
3381670	French Guiana	French Guiana	GF	GUF	195506	91000	EUR	Euro	€	fr-GF	594	gf	#####	^((97|98)3\\d{2})$	Cayenne	2018-12-03 23:28:50.535604-05	2020-09-13 16:03:33.686078-04	french-guiana	6255150	Q3769
7626836	Curacao	Curacao	CW	CUW	141766	444	ANG	Guilder	ƒ	nl,pap	599	cw			 Willemstad	2018-12-03 23:28:50.466007-05	2020-09-13 16:19:08.321546-04	curacao	6255149	Q25279
2309096	Equatorial Guinea	Equatorial Guinea	GQ	GNQ	1014999	28051	XAF	Franc	Fr	es-GQ,fr	240	gq			Malabo	2018-12-03 23:28:50.557545-05	2020-09-13 15:55:45.849112-04	equatorial-guinea	6255146	Q983
3077311	Czechia	Czechia	CZ	CZE	10476000	78866	CZK	Koruna	Kč	cs,sk	420	cz	### ##	^\\d{3}\\s?\\d{2}$	Prague	2018-12-03 23:28:50.473613-05	2020-09-13 16:01:25.510971-04	czechia	6255148	Q213
2921044	Germany	Germany	DE	DEU	81802257	357021	EUR	Euro	€	de	49	de	#####	^(\\d{5})$	Berlin	2018-12-03 23:28:50.476135-05	2020-09-13 15:59:41.819658-04	germany	6255148	Q183
3374766	Cape Verde	Cape Verde	CV	CPV	508659	4033	CVE	Escudo	$, Esc	pt-CV	238	cv	####	^(\\d{4})$	Praia	2018-12-03 23:28:50.463346-05	2020-09-13 16:03:32.417125-04	cape-verde	6255146	Q1011
3580239	Grenada	Grenada	GD	GRD	107818	344	XCD	Dollar	$	en-GD	+1-473	gd			St. George's	2018-12-03 23:28:50.530799-05	2020-09-13 16:04:25.360818-04	grenada	6255149	Q769
2420477	Guinea	Guinea	GN	GIN	10324025	245857	GNF	Franc	Fr	fr-GN	224	gn			Conakry	2018-12-03 23:28:50.552248-05	2020-09-13 15:56:14.416829-04	guinea	6255146	Q1006
1547314	Heard Island and McDonald Islands	Heard Island and McDonald Islands	HM	HMD	0	412	AUD	Dollar	$		 	hm				2018-12-03 23:28:50.578272-05	2020-09-13 15:51:44.631695-04	heard-island-and-mcdonald-islands	6255152	Q131198
1819730	Hong Kong	Hong Kong	HK	HKG	6898686	1092	HKD	Dollar	$	zh-HK,yue,zh,en	852	hk			Hong Kong	2018-12-03 23:28:50.575634-05	2020-09-13 15:53:12.240592-04	hong-kong	6255147	Q8646
2078138	Christmas Island	Christmas Island	CX	CXR	1500	135	AUD	Dollar	$	en,zh,ms-CC	61	cx	####	^(\\d{4})$	Flying Fish Cove	2018-12-03 23:28:50.46849-05	2020-09-13 15:54:25.074011-04	christmas-island	6255151	Q31063
2081918	Micronesia	Micronesia	FM	FSM	107708	702	USD	Dollar	$	en-FM,chk,pon,yap,kos,uli,woe,nkr,kpg	691	fm	#####	^(\\d{5})$	Palikir	2018-12-03 23:28:50.517545-05	2020-09-13 15:54:26.468728-04	micronesia	6255151	Q702
2372248	Guinea-Bissau	Guinea-Bissau	GW	GNB	1565126	36120	XOF	Franc	Fr	pt-GW,pov	245	gw	####	^(\\d{4})$	Bissau	2018-12-03 23:28:50.570472-05	2020-09-13 15:55:56.470629-04	guinea-bissau	6255146	Q1007
2411586	Gibraltar	Gibraltar	GI	GIB	27884	6	GIP	Pound	£	en-GI,es,it,pt	350	gi			Gibraltar	2018-12-03 23:28:50.544287-05	2020-09-13 15:56:11.444321-04	gibraltar	6255148	Q1410
2413451	Gambia	Gambia	GM	GMB	1593256	11300	GMD	Dalasi	D	en-GM,mnk,wof,wo,ff	220	gm			Banjul	2018-12-03 23:28:50.549678-05	2020-09-13 15:56:12.210955-04	gambia	6255146	Q1005
2589581	Algeria	Algeria	DZ	DZA	34586184	2381740	DZD	Dinar	د.ج	ar-DZ	213	dz	#####	^(\\d{5})$	Algiers	2018-12-03 23:28:50.489247-05	2020-09-13 15:57:20.896579-04	algeria	6255146	Q262
2623032	Denmark	Denmark	DK	DNK	5484000	43094	DKK	Krone	kr	da-DK,en,fo,de-DK	45	dk	####	^(\\d{4})$	Copenhagen	2018-12-03 23:28:50.48142-05	2020-09-13 15:57:33.946133-04	denmark	6255148	Q35
3474414	Falkland Islands	Falkland Islands	FK	FLK	2638	12173	FKP	Pound	£	en-FK	500	fk			Stanley	2018-12-03 23:28:50.514955-05	2020-09-13 16:04:05.53479-04	falkland-islands	6255150	Q9648
3474415	South Georgia and the South Sandwich Islands	South Georgia and the South Sandwich Islands	GS	SGS	30	3903	GBP	Pound	£	en		gs			Grytviken	2018-12-03 23:28:50.562906-05	2020-09-13 16:04:05.53702-04	south-georgia-and-the-south-sandwich-islands	6255152	Q35086
3508796	Dominican Republic	Dominican Republic	DO	DOM	9823821	48730	DOP	Peso	$	es-DO	+1-809 and 1-829	do	#####	^(\\d{5})$	Santo Domingo	2018-12-03 23:28:50.486574-05	2020-09-13 16:04:11.024156-04	dominican-republic	6255149	Q786
3575830	Dominica	Dominica	DM	DMA	72813	754	XCD	Dollar	$	en-DM	+1-767	dm			Roseau	2018-12-03 23:28:50.484023-05	2020-09-13 16:04:24.678211-04	dominica	6255149	Q784
357994	Egypt	Egypt	EG	EGY	80471869	1001450	EGP	Pound	£,ج.م	ar-EG,en,fr	20	eg	#####	^(\\d{5})$	Cairo	2018-12-03 23:28:50.496466-05	2020-09-13 16:04:25.30584-04	egypt	6255146	Q79
4043988	Guam	Guam	GU	GUM	159358	549	USD	Dollar	$	en-GU,ch-GU	+1-671	gu	969##	^(969\\d{2})$	Hagatna	2018-12-03 23:28:50.567919-05	2020-09-13 16:05:40.88754-04	guam	6255151	Q16635
453733	Estonia	Estonia	EE	EST	1291170	45226	EUR	Euro	€	et,ru	372	ee	#####	^(\\d{5})$	Tallinn	2018-12-03 23:28:50.494005-05	2020-09-13 16:07:16.38552-04	estonia	6255148	Q191
614540	Georgia	Georgia	GE	GEO	4630000	69700	GEL	Lari	ლ	ka,ru,hy,az	995	ge	####	^(\\d{4})$	Tbilisi	2018-12-03 23:28:50.533221-05	2020-09-13 16:11:42.541783-04	georgia	6255147	Q230
660013	Finland	Finland	FI	FIN	5244000	337030	EUR	Euro	€	fi-FI,sv-FI,smn	358	fi	#####	^(?:FI)*(\\d{5})$	Helsinki	2018-12-03 23:28:50.509913-05	2020-09-13 16:15:32.528263-04	finland	6255148	Q33
2275384	Liberia	Liberia	LR	LBR	3685076	111370	LRD	Dollar	$	en-LR	231	lr	####	^(\\d{4})$	Monrovia	2018-12-03 23:28:50.675261-05	2020-09-13 15:55:38.659353-04	liberia	6255146	Q1014
99237	Iraq	Iraq	IQ	IRQ	29671605	437072	IQD	Dinar	ع.د	ar-IQ,ku,hy	964	iq	#####	^(\\d{5})$	Baghdad	2018-12-03 23:28:50.606622-05	2020-09-13 16:22:59.37862-04	iraq	6255147	Q796
921929	Comoros	Comoros	KM	COM	773407	2170	KMF	Franc	Fr	ar,fr-KM	269	km			Moroni	2018-12-03 23:28:50.638377-05	2020-09-13 16:22:35.115961-04	comoros	6255146	Q970
2629691	Iceland	Iceland	IS	ISL	308910	103000	ISK	Krona	kr	is,en,de,da,sv,no	354	is	###	^(\\d{3})$	Reykjavik	2018-12-03 23:28:50.611895-05	2020-09-13 15:57:36.513215-04	iceland	6255148	Q189
1835841	South Korea	South Korea	KR	KOR	48422644	98480	KRW	Won	₩	ko-KR,en	82	kr	SEOUL ###-###	^(?:SEOUL)*(\\d{6})$	Seoul	2018-12-03 23:28:50.646552-05	2020-09-13 15:53:16.448048-04	south-korea	6255147	Q884
1227603	Sri Lanka	Sri Lanka	LK	LKA	21513990	65610	LKR	Rupee	ரூ	si,ta,en	94	lk	#####	^(\\d{5})$	Colombo	2018-12-03 23:28:50.672546-05	2020-09-13 15:49:49.55994-04	sri-lanka	6255147	Q854
932692	Lesotho	Lesotho	LS	LSO	1919552	30355	LSL	Loti	L	en-LS,st,zu,xh	266	ls	###	^(\\d{3})$	Maseru	2018-12-03 23:28:50.677843-05	2020-09-13 16:22:38.720971-04	lesotho	6255146	Q1013
2215636	Libya	Libya	LY	LBY	6461454	1759540	LYD	Dinar	ل.د	ar-LY,it,en	218	ly			Tripoli	2018-12-03 23:28:50.689117-05	2020-09-13 15:55:17.207763-04	libya	6255146	Q1016
2080185	Marshall Islands	Marshall Islands	MH	MHL	65859	181	USD	Dollar	$	mh,en-MH	692	mh	#####-####	^969\\d{2}(-\\d{4})$	Majuro	2018-12-03 23:28:50.708005-05	2020-09-13 15:54:26.00632-04	marshall-islands	6255151	Q709
3578421	Saint Martin	Saint Martin	MF	MAF	35925	53	EUR	Euro	€	fr	590	gp	#####	^(\\d{5})$	Marigot	2018-12-03 23:28:50.702852-05	2020-09-13 16:04:25.102975-04	saint-martin	6255149	Q126125
1282588	British Indian Ocean Territory	British Indian Ocean Territory	IO	IOT	4000	60	USD	Dollar	$	en-IO	246	io			Diego Garcia	2018-12-03 23:28:50.603904-05	2020-09-13 15:50:10.499037-04	british-indian-ocean-territory	6255147	Q43448
3575174	Saint Kitts and Nevis	Saint Kitts and Nevis	KN	KNA	51134	261	XCD	Dollar	$	en-KN	+1-869	kn			Basseterre	2018-12-03 23:28:50.640981-05	2020-09-13 16:04:24.519758-04	saint-kitts-and-nevis	6255149	Q763
1522867	Kazakhstan	Kazakhstan	KZ	KAZ	15340000	2717300	KZT	Tenge	Т	kk,ru	7	kz	######	^(\\d{6})$	Astana	2018-12-03 23:28:50.658245-05	2020-09-13 15:51:33.619325-04	kazakhstan	6255147	Q232
3576468	Saint Lucia	Saint Lucia	LC	LCA	160922	616	XCD	Dollar	$	en-LC	+1-758	lc			Castries	2018-12-03 23:28:50.666698-05	2020-09-13 16:04:24.768524-04	saint-lucia	6255149	Q760
1643084	Indonesia	Indonesia	ID	IDN	242968342	1919440	IDR	Rupiah	Rp	id,en,nl,jv	62	id	#####	^(\\d{5})$	Jakarta	2018-12-03 23:28:50.591146-05	2020-09-13 15:52:09.640367-04	indonesia	6255147	Q252
4030945	Kiribati	Kiribati	KI	KIR	92533	811	AUD	Dollar	$	en-KI,gil	686	ki			Tarawa	2018-12-03 23:28:50.635945-05	2020-09-13 16:05:38.88305-04	kiribati	6255151	Q710
1831722	Cambodia	Cambodia	KH	KHM	14453680	181040	KHR	Riels	៛	km,fr,en	855	kh	#####	^(\\d{5})$	Phnom Penh	2018-12-03 23:28:50.633562-05	2020-09-13 15:53:15.316908-04	cambodia	6255147	Q424
2542007	Morocco	Morocco	MA	MAR	33848242	446550	MAD	Dirham	د.م.	ar-MA,ber,fr	212	ma	#####	^(\\d{5})$	Rabat	2018-12-03 23:28:50.691942-05	2020-09-13 15:57:05.241805-04	morocco	6255146	Q1028
1269750	India	India	IN	IND	1173108018	3287590	INR	Rupee	₨	en-IN,hi,bn,te,mr,ta,ur,gu,kn,ml,or,pa,as,bh,sat,ks,ne,sd,kok,doi,mni,sit,sa,fr,lus,inc	91	in	######	^(\\d{6})$	New Delhi	2018-12-03 23:28:50.601414-05	2020-09-13 15:50:03.337126-04	india	6255147	Q668
1861060	Japan	Japan	JP	JPN	127288000	377835	JPY	Yen	¥	ja	81	jp	###-####	^\\d{3}-\\d{4}$	Tokyo	2018-12-03 23:28:50.625223-05	2020-09-13 15:53:27.383769-04	japan	6255147	Q17
285570	Kuwait	Kuwait	KW	KWT	2789132	17820	KWD	Dinar	د.ك	ar-KW,en	965	kw	#####	^(\\d{5})$	Kuwait City	2018-12-03 23:28:50.651888-05	2020-09-13 15:59:17.299987-04	kuwait	6255147	Q817
1062947	Madagascar	Madagascar	MG	MDG	21281844	587040	MGA	Ariary	Ar	fr-MG,mg	261	mg	###	^(\\d{3})$	Antananarivo	2018-12-03 23:28:50.705383-05	2020-09-13 15:48:27.178958-04	madagascar	6255146	Q1019
1873107	North Korea	North Korea	KP	PRK	22912177	120540	KPW	Won	₩	ko-KP	850	kp	###-###	^(\\d{6})$	Pyongyang	2018-12-03 23:28:50.643386-05	2020-09-13 15:53:31.266624-04	north-korea	6255147	Q423
597427	Lithuania	Lithuania	LT	LTU	2944459	65200	EUR	Euro	€	lt,ru,pl	370	lt	LT-#####	^(?:LT)*(\\d{5})$	Vilnius	2018-12-03 23:28:50.680615-05	2020-09-13 16:11:05.540222-04	lithuania	6255148	Q37
3489940	Jamaica	Jamaica	JM	JAM	2847232	10991	JMD	Dollar	$	en-JM	+1-876	jm			Kingston	2018-12-03 23:28:50.619555-05	2020-09-13 16:04:07.620165-04	jamaica	6255149	Q766
3202326	Croatia	Croatia	HR	HRV	4284889	56542	HRK	Kuna	kn	hr-HR,sr	385	hr	#####	^(?:HR)*(\\d{5})$	Zagreb	2018-12-03 23:28:50.583459-05	2020-09-13 16:03:07.597223-04	croatia	6255148	Q224
1655842	Laos	Laos	LA	LAO	6368162	236800	LAK	Kip	₭	lo,fr,en	856	la	#####	^(\\d{5})$	Vientiane	2018-12-03 23:28:50.660974-05	2020-09-13 15:52:14.252162-04	laos	6255147	Q819
1527747	Kyrgyzstan	Kyrgyzstan	KG	KGZ	5776500	198500	KGS	Som	лв	ky,uz,ru	996	kg	######	^(\\d{6})$	Bishkek	2018-12-03 23:28:50.631087-05	2020-09-13 15:51:35.007992-04	kyrgyzstan	6255147	Q813
3194884	Montenegro	Montenegro	ME	MNE	666730	14026	EUR	Euro	€	sr,hu,bs,sq,hr,rom	382	me	#####	^(\\d{5})$	Podgorica	2018-12-03 23:28:50.70028-05	2020-09-13 16:03:05.008756-04	montenegro	6255148	Q236
248816	Jordan	Jordan	JO	JOR	6407085	92300	JOD	Dinar	د.ا	ar-JO,en	962	jo	#####	^(\\d{5})$	Amman	2018-12-03 23:28:50.622082-05	2020-09-13 15:56:35.553272-04	jordan	6255147	Q810
130758	Iran	Iran	IR	IRN	76923300	1648000	IRR	Rial	﷼	fa-IR,ku	98	ir	##########	^(\\d{10})$	Tehran	2018-12-03 23:28:50.609272-05	2020-09-13 15:50:16.998864-04	iran	6255147	Q794
272103	Lebanon	Lebanon	LB	LBN	4125247	10400	LBP	Pound	ل.ل	ar-LB,fr-LB,en,hy	961	lb	#### ####|####	^(\\d{4}(\\d{4})?)$	Beirut	2018-12-03 23:28:50.66375-05	2020-09-13 15:58:16.460308-04	lebanon	6255147	Q822
2960313	Luxembourg	Luxembourg	LU	LUX	497538	2586	EUR	Euro	€	lb,de-LU,fr-LU	352	lu	L-####	^(?:L-)?\\d{4}$	Luxembourg	2018-12-03 23:28:50.683326-05	2020-09-13 15:59:56.731547-04	luxembourg	6255148	Q32
2993457	Monaco	Monaco	MC	MCO	32965	1	EUR	Euro	€	fr-MC,en,it	377	mc	#####	^(\\d{5})$	Monaco	2018-12-03 23:28:50.695-05	2020-09-13 16:00:17.615992-04	monaco	6255148	Q235
3042058	Liechtenstein	Liechtenstein	LI	LIE	35000	160	CHF	Franc	Fr	de-LI	423	li	####	^(\\d{4})$	Vaduz	2018-12-03 23:28:50.669687-05	2020-09-13 16:00:44.983905-04	liechtenstein	6255148	Q347
3042225	Isle of Man	Isle of Man	IM	IMN	75049	572	GBP	Pound	£	en,gv	+44-1624	im	@# #@@|@## #@@|@@# #@@|@@## #@@|@#@ #@@|@@#@ #@@|GIR0AA	^((?:(?:[A-PR-UWYZ][A-HK-Y]\\d[ABEHMNPRV-Y0-9]|[A-PR-UWYZ]\\d[A-HJKPS-UW0-9])\\s\\d[ABD-HJLNP-UW-Z]{2})|GIR\\s?0AA)$	Douglas	2018-12-03 23:28:50.598772-05	2020-09-13 16:00:45.043844-04	isle-of-man	6255148	Q9676
3175395	Italy	Italy	IT	ITA	60340328	301230	EUR	Euro	€	it-IT,de-IT,fr-IT,sc,ca,co,sl	39	it	#####	^(\\d{5})$	Rome	2018-12-03 23:28:50.614464-05	2020-09-13 16:02:58.715527-04	italy	6255148	Q38
3580718	Cayman Islands	Cayman Islands	KY	CYM	44270	262	KYD	Dollar	$	en-KY	+1-345	ky			George Town	2018-12-03 23:28:50.654458-05	2020-09-13 16:04:25.511258-04	cayman-islands	6255149	Q5785
3723988	Haiti	Haiti	HT	HTI	9648924	27750	HTG	Gourde	G	ht,fr-HT	509	ht	HT####	^(?:HT)*(\\d{4})$	Port-au-Prince	2018-12-03 23:28:50.586006-05	2020-09-13 16:04:52.298003-04	haiti	6255149	Q790
458258	Latvia	Latvia	LV	LVA	2217969	64589	EUR	Euro	€	lv,ru,lt	371	lv	LV-####	^(?:LV)*(\\d{4})$	Riga	2018-12-03 23:28:50.686233-05	2020-09-13 16:07:23.085199-04	latvia	6255148	Q211
617790	Moldova	Moldova	MD	MDA	4324000	33843	MDL	Leu	L	ro,ru,gag,tr	373	md	MD-####	^MD-\\d{4}$	Chisinau	2018-12-03 23:28:50.69774-05	2020-09-13 16:11:50.388606-04	moldova	6255148	Q217
718075	Macedonia	Macedonia	MK	MKD	2062294	25333	MKD	Denar	ден	mk,sq,tr,rmm,sr	389	mk	####	^(\\d{4})$	Skopje	2018-12-03 23:28:50.710433-05	2020-09-13 16:17:27.06306-04	macedonia	6255148	Q221
719819	Hungary	Hungary	HU	HUN	9982000	93030	HUF	Forint	Ft	hu-HU	36	hu	####	^(\\d{4})$	Budapest	2018-12-03 23:28:50.588606-05	2020-09-13 16:17:28.645496-04	hungary	6255148	Q28
831053	Kosovo	Kosovo	XK	XKX	1800000	10908	EUR	Euro	€	sq,sr					Pristina	2018-12-03 23:28:50.649344-05	2020-09-13 16:20:55.583766-04	kosovo	6255148	Q1246
6290252	Serbia	Serbia	RS	SRB	7344847	88361	RSD	Dinar	RSD	sr,hu,bs,rom	381	rs	######	^(\\d{6})$	Belgrade	2018-12-03 23:28:50.835318-05	2020-09-13 16:11:59.033072-04	serbia	6255148	Q403
286963	Oman	Oman	OM	OMN	2967717	212460	OMR	Rial	ر.ع.	ar-OM,en,bal,ur	968	om	###	^(\\d{3})$	Muscat	2018-12-03 23:28:50.787955-05	2020-09-13 15:59:22.071088-04	oman	6255147	Q842
927384	Malawi	Malawi	MW	MWI	15447500	118480	MWK	Kwacha	MK	ny,yao,tum,swk	265	mw			Lilongwe	2018-12-03 23:28:50.744189-05	2020-09-13 16:22:36.804686-04	malawi	6255146	Q1020
934292	Mauritius	Mauritius	MU	MUS	1294104	2040	MUR	Rupee	₨	en-MU,bho,fr	230	mu			Port Louis	2018-12-03 23:28:50.738119-05	2020-09-13 16:22:39.733694-04	mauritius	6255146	Q1027
935317	Reunion	Reunion	RE	REU	776948	2517	EUR	Euro	€	fr-RE	262	re	#####	^((97|98)(4|7|8)\\d{2})$	Saint-Denis	2018-12-03 23:28:50.830395-05	2020-09-13 16:22:40.597313-04	reunion	6255146	Q17070
3424932	Saint Pierre and Miquelon	Saint Pierre and Miquelon	PM	SPM	7012	242	EUR	Euro	€	fr-PM	508	pm	#####	^(97500)$	Saint-Pierre	2018-12-03 23:28:50.808813-05	2020-09-13 16:03:43.058852-04	saint-pierre-and-miquelon	6255149	Q34617
6254930	Palestinian Territory	Palestinian Territory	PS	PSE	3800000	5970	ILS	Shekel	₪	ar-PS	970	ps			East Jerusalem	2018-12-03 23:28:50.816706-05	2020-09-13 22:20:29.01713-04	palestinian-territory	6255147	Q219060
2562770	Malta	Malta	MT	MLT	403000	316	EUR	Euro	€	mt,en-MT	356	mt	@@@ ####	^[A-Z]{3}\\s?\\d{4}$	Valletta	2018-12-03 23:28:50.734801-05	2020-09-13 15:57:11.917183-04	malta	6255148	Q233
3996063	Mexico	Mexico	MX	MEX	112468855	1972550	MXN	Peso	$	es-MX	52	mx	#####	^(\\d{5})$	Mexico City	2018-12-03 23:28:50.747794-05	2020-09-13 16:05:34.307219-04	mexico	6255149	Q96
2264397	Portugal	Portugal	PT	PRT	10676000	92391	EUR	Euro	€	pt-PT,mwl	351	pt	####-###	^\\d{4}-\\d{3}\\s?[a-zA-Z]{0,25}$	Lisbon	2018-12-03 23:28:50.819276-05	2020-09-13 15:55:36.2092-04	portugal	6255148	Q45
3570311	Martinique	Martinique	MQ	MTQ	432900	1100	EUR	Euro	€	fr-MQ	596	mq	#####	^(\\d{5})$	Fort-de-France	2018-12-03 23:28:50.725961-05	2020-09-13 16:04:23.951304-04	martinique	6255149	Q17054
3703430	Panama	Panama	PA	PAN	3410676	78200	PAB	Balboa	B/.	es-PA,en	507	pa			Panama City	2018-12-03 23:28:50.790631-05	2020-09-13 16:04:50.136155-04	panama	6255149	Q804
2110425	Nauru	Nauru	NR	NRU	10065	21	AUD	Dollar	$	na,en-NR	674	nr			Yaren	2018-12-03 23:28:50.779948-05	2020-09-13 15:54:32.150019-04	nauru	6255151	Q697
2103350	Solomon Islands	Solomon Islands	SB	SLB	559198	28450	SBD	Dollar	$	en-SB,tpi	677	sb			Honiara	2018-12-03 23:28:50.846877-05	2020-09-13 15:54:29.963716-04	solomon-islands	6255151	Q685
1694008	Philippines	Philippines	PH	PHL	99900177	300000	PHP	Peso	₱	tl,en-PH,fil,ceb,tgl,ilo,hil,war,pam,bik,bcl,pag,mrw,tsg,mdh,cbk,krj,sgd,msb,akl,ibg,yka,mta,abx	63	ph	####	^(\\d{4})$	Manila	2018-12-03 23:28:50.800942-05	2020-09-13 15:52:27.017208-04	philippines	6255147	Q928
2139685	New Caledonia	New Caledonia	NC	NCL	216494	19060	XPF	Franc	Fr	fr-NC	687	nc	#####	^(\\d{5})$	Noumea	2018-12-03 23:28:50.758782-05	2020-09-13 15:54:43.875738-04	new-caledonia	6255151	Q33788
2088628	Papua New Guinea	Papua New Guinea	PG	PNG	6064515	462840	PGK	Kina	K	en-PG,ho,meu,tpi	675	pg	###	^(\\d{3})$	Port Moresby	2018-12-03 23:28:50.798269-05	2020-09-13 15:54:27.543767-04	papua-new-guinea	6255151	Q691
1282988	Nepal	Nepal	NP	NPL	28951852	140800	NPR	Rupee	₨	ne,en	977	np	#####	^(\\d{5})$	Kathmandu	2018-12-03 23:28:50.777536-05	2020-09-13 15:50:10.630436-04	nepal	6255147	Q837
289688	Qatar	Qatar	QA	QAT	840926	11437	QAR	Rial	ر.ق	ar-QA,es	974	qa			Doha	2018-12-03 23:28:50.827267-05	2020-09-13 15:59:33.032693-04	qatar	6255147	Q846
1282028	Maldives	Maldives	MV	MDV	395650	300	MVR	Rufiyaa	ރ.	dv,en	960	mv	#####	^(\\d{5})$	Male	2018-12-03 23:28:50.741219-05	2020-09-13 15:50:10.071486-04	maldives	6255147	Q826
1559582	Palau	Palau	PW	PLW	19907	458	USD	Dollar	$	pau,sov,en-PW,tox,ja,fil,zh	680	pw	96940	^(96940)$	Melekeok	2018-12-03 23:28:50.821965-05	2020-09-13 15:51:46.375015-04	palau	6255151	Q695
102358	Saudi Arabia	Saudi Arabia	SA	SAU	25731776	1960582	SAR	Rial	ر.س	ar-SA	966	sa	#####	^(\\d{5})$	Riyadh	2018-12-03 23:28:50.84389-05	2020-09-13 15:47:57.693327-04	saudi-arabia	6255147	Q851
2378080	Mauritania	Mauritania	MR	MRT	3205060	1030700	MRO	Ouguiya	UM	ar-MR,fuc,snk,fr,mey,wo	222	mr			Nouakchott	2018-12-03 23:28:50.728658-05	2020-09-13 15:55:58.165039-04	mauritania	6255146	Q1025
3355338	Namibia	Namibia	NA	NAM	2128471	825418	NAD	Dollar	$	en-NA,af,de,hz,naq	264	na			Windhoek	2018-12-03 23:28:50.755984-05	2020-09-13 16:03:29.839029-04	namibia	6255146	Q1030
1821275	Macao	Macao	MO	MAC	449198	254	MOP	Pataca	P	zh,zh-MO,pt	853	mo			Macao	2018-12-03 23:28:50.720765-05	2020-09-13 15:53:12.948525-04	macao	6255147	Q14773
798549	Romania	Romania	RO	ROU	21959278	237500	RON	Leu	RON	ro,hu,rom	40	ro	######	^(\\d{6})$	Bucharest	2018-12-03 23:28:50.832853-05	2020-09-13 16:20:16.380637-04	romania	6255148	Q218
2440476	Niger	Niger	NE	NER	15878271	1267000	XOF	Franc	Fr	fr-NE,ha,kr,dje	227	ne	####	^(\\d{4})$	Niamey	2018-12-03 23:28:50.761474-05	2020-09-13 15:56:19.747508-04	niger	6255146	Q1032
4030699	Pitcairn	Pitcairn	PN	PCN	46	47	NZD	Dollar	$	en-PN	870	pn			Adamstown	2018-12-03 23:28:50.81146-05	2020-09-13 16:05:38.829097-04	pitcairn	6255151	Q35672
1168579	Pakistan	Pakistan	PK	PAK	184404791	803940	PKR	Rupee	₨	ur-PK,en-PK,pa,sd,ps,brh	92	pk	#####	^(\\d{5})$	Islamabad	2018-12-03 23:28:50.803486-05	2020-09-13 15:49:27.898291-04	pakistan	6255147	Q843
1036973	Mozambique	Mozambique	MZ	MOZ	22061451	801590	MZN	Metical	MT	pt-MZ,vmw	258	mz	####	^(\\d{4})$	Maputo	2018-12-03 23:28:50.75326-05	2020-09-13 15:48:08.503549-04	mozambique	6255146	Q1029
1327865	Myanmar	Myanmar	MM	MMR	53414374	678500	MMK	Kyat	K	my	95	mm	#####	^(\\d{5})$	Nay Pyi Taw	2018-12-03 23:28:50.715623-05	2020-09-13 15:50:21.612049-04	myanmar	6255147	Q836
2017370	Russia	Russia	RU	RUS	140702000	17100000	RUB	Ruble	р.	ru,tt,xal,cau,ady,kv,ce,tyv,cv,udm,tut,mns,bua,myv,mdf,chm,ba,inh,tut,kbd,krc,av,sah,nog	7	ru	######	^(\\d{6})$	Moscow	2018-12-03 23:28:50.838174-05	2020-09-13 15:54:00.604557-04	russia	6255148	Q159
2029969	Mongolia	Mongolia	MN	MNG	3086918	1565000	MNT	Tugrik	₮	mn,ru	976	mn	######	^(\\d{6})$	Ulan Bator	2018-12-03 23:28:50.718144-05	2020-09-13 15:54:08.854845-04	mongolia	6255147	Q711
2155115	Norfolk Island	Norfolk Island	NF	NFK	1828	34	AUD	Dollar	$	en-NF	672	nf	####	^(\\d{4})$	Kingston	2018-12-03 23:28:50.764088-05	2020-09-13 15:54:49.854803-04	norfolk-island	6255151	Q31057
2186224	New Zealand	New Zealand	NZ	NZL	4252277	268680	NZD	Dollar	$	en-NZ,mi	64	nz	####	^(\\d{4})$	Wellington	2018-12-03 23:28:50.785383-05	2020-09-13 15:55:03.078084-04	new-zealand	6255151	Q664
2750405	Netherlands	Netherlands	NL	NLD	16645000	41526	EUR	Euro	€	nl-NL,fy-NL	31	nl	#### @@	^(\\d{4}[A-Z]{2})$	Amsterdam	2018-12-03 23:28:50.772214-05	2020-09-13 15:58:29.725965-04	netherlands	6255148	Q29999
3437598	Paraguay	Paraguay	PY	PRY	6375830	406750	PYG	Guarani	₲	es-PY,gn	595	py	####	^(\\d{4})$	Asuncion	2018-12-03 23:28:50.824625-05	2020-09-13 16:03:46.82148-04	paraguay	6255150	Q733
3578097	Montserrat	Montserrat	MS	MSR	9341	102	XCD	Dollar	$	en-MS	+1-664	ms			Plymouth	2018-12-03 23:28:50.731242-05	2020-09-13 16:04:25.042148-04	montserrat	6255149	Q13353
3617476	Nicaragua	Nicaragua	NI	NIC	5995928	129494	NIO	Cordoba	C$	es-NI,en	505	ni	###-###-#	^(\\d{7})$	Managua	2018-12-03 23:28:50.769492-05	2020-09-13 16:04:33.60122-04	nicaragua	6255149	Q811
3932488	Peru	Peru	PE	PER	29907003	1285220	PEN	Sol	S/.	es-PE,qu,ay	51	pe			Lima	2018-12-03 23:28:50.793132-05	2020-09-13 16:05:23.918257-04	peru	6255150	Q419
4036232	Niue	Niue	NU	NIU	2166	260	NZD	Dollar	$	niu,en-NU	683	nu			Alofi	2018-12-03 23:28:50.782676-05	2020-09-13 16:05:40.035266-04	niue	6255151	Q34020
4041468	Northern Mariana Islands	Northern Mariana Islands	MP	MNP	53883	477	USD	Dollar	$	fil,tl,zh,ch-MP,en-MP	+1-670	mp	#####	^9695\\d{1}$	Saipan	2018-12-03 23:28:50.723358-05	2020-09-13 16:05:40.586218-04	northern-mariana-islands	6255151	Q16644
4566966	Puerto Rico	Puerto Rico	PR	PRI	3916632	9104	USD	Dollar	$	en-PR,es-PR	+1-787 and 1-939	pr	#####-####	^00[679]\\d{2}(?:-\\d{4})?$	San Juan	2018-12-03 23:28:50.814091-05	2020-09-13 16:07:21.439756-04	puerto-rico	6255149	Q1183
49518	Rwanda	Rwanda	RW	RWA	11055976	26338	RWF	Franc	Fr	rw,en-RW,fr-RW,sw	250	rw			Kigali	2018-12-03 23:28:50.841099-05	2020-09-13 16:08:17.922663-04	rwanda	6255146	Q1037
798544	Poland	Poland	PL	POL	38500000	312685	PLN	Zloty	zł	pl	48	pl	##-###	^\\d{2}-\\d{3}$	Warsaw	2018-12-03 23:28:50.806318-05	2020-09-13 16:20:16.378923-04	poland	6255148	Q36
4031074	Tokelau	Tokelau	TK	TKL	1466	10	NZD	Dollar	$	tkl,en-TK	690	tk				2018-12-03 23:28:50.915074-05	2020-09-13 16:05:38.895084-04	tokelau	6255151	Q36823
1546748	French Southern Territories	French Southern Territories	TF	ATF	140	7829	EUR	Euro  	€	fr		tf			Port-aux-Francais	2018-12-03 23:28:50.904533-05	2020-09-13 15:51:44.336399-04	french-southern-territories	6255152	Q129003
5854968	United States Minor Outlying Islands	United States Minor Outlying Islands	UM	UMI	0	0	USD	Dollar 	$	en-UM	1	um				2018-12-03 23:28:50.947064-05	2020-09-13 16:10:40.417807-04	united-states-minor-outlying-islands	6255151	Q16645
934841	Swaziland	Swaziland	SZ	SWZ	1354051	17363	SZL	Lilangeni	L	en-SZ,ss-SZ	268	sz	@###	^([A-Z]\\d{3})$	Mbabane	2018-12-03 23:28:50.896039-05	2020-09-13 16:22:40.11325-04	swaziland	6255146	Q1050
4796775	U.S. Virgin Islands	U.S. Virgin Islands	VI	VIR	108708	352	USD	Dollar	$	en-VI	+1-340	vi	#####-####	^008\\d{2}(?:-\\d{4})?$	Charlotte Amalie	2018-12-03 23:28:50.969922-05	2020-09-13 22:20:29.012216-04	us-virgin-islands	6255149	Q11703
607072	Svalbard and Jan Mayen	Svalbard and Jan Mayen	SJ	SJM	2550	62049	NOK	Krone	kr	no,ru	47	sj	####	^(\\d{4})$	Longyearbyen	2018-12-03 23:28:50.86793-05	2020-09-13 22:20:29.014598-04	svalbard-and-jan-mayen	6255148	Q842829
2110297	Tuvalu	Tuvalu	TV	TUV	10472	26	AUD	Dollar	$	tvl,en,sm,gil	688	tv			Funafuti	2018-12-03 23:28:50.933812-05	2020-09-13 15:54:32.088501-04	tuvalu	6255151	Q672
366755	Sudan	Sudan	SD	SDN	35000000	1861484	SDG	Pound	S$	ar-SD,en,fia	249	sd	#####	^(\\d{5})$	Khartoum	2018-12-03 23:28:50.85205-05	2020-09-13 16:04:42.356734-04	sudan	6255146	Q1049
1880251	Singapore	Singapore	SG	SGP	4701069	692	SGD	Dollar	$	cmn,en-SG,ms-SG,ta-SG,zh-SG	65	sg	######	^(\\d{6})$	Singapore	2018-12-03 23:28:50.860311-05	2020-09-13 15:53:32.624754-04	singapore	6255147	Q334
3144096	Norway	Norway	NO	NOR	5009150	324220	NOK	Krone	kr	no,nb,nn,se,fi	47	no	####	^(\\d{4})$	Oslo	2018-12-03 23:28:50.774881-05	2020-09-13 16:02:52.409412-04	norway	6255148	Q20
2661886	Sweden	Sweden	SE	SWE	9828655	449964	SEK	Krona	kr	sv-SE,se,sma,fi-SE	46	se	### ##	^(?:SE)?\\d{3}\\s\\d{2}$	Stockholm	2018-12-03 23:28:50.857669-05	2020-09-13 15:57:59.916837-04	sweden	6255148	Q34
1605651	Thailand	Thailand	TH	THA	67089500	514000	THB	Baht	฿	th,en	66	th	#####	^(\\d{5})$	Bangkok	2018-12-03 23:28:50.909794-05	2020-09-13 15:51:55.759439-04	thailand	6255147	Q869
3577718	British Virgin Islands	British Virgin Islands	VG	VGB	21730	153	USD	Dollar	$	en-VG	+1-284	vg			Road Town	2018-12-03 23:28:50.96607-05	2020-09-13 16:04:24.955211-04	british-virgin-islands	6255149	Q25305
3585968	El Salvador	El Salvador	SV	SLV	6052064	21040	USD	Dollar	$	es-SV	503	sv	CP ####	^(?:CP)*(\\d{4})$	San Salvador	2018-12-03 23:28:50.888484-05	2020-09-13 16:04:27.252117-04	el-salvador	6255149	Q792
3625428	Venezuela	Venezuela	VE	VEN	27223228	912050	VES	Bolivar Soberano	\N	es-VE	58	ve	####	^(\\d{4})$	Caracas	2018-12-03 23:28:50.963189-05	2020-09-13 16:04:35.245501-04	venezuela	6255150	Q717
1668284	Taiwan	Taiwan	TW	TWN	22894384	35980	TWD	Dollar	$	zh-TW,zh,nan,hak	886	tw	#####	^(\\d{5})$	Taipei	2018-12-03 23:28:50.936412-05	2020-09-13 15:52:16.44546-04	taiwan	6255147	Q865
149590	Tanzania	Tanzania	TZ	TZA	41892895	945087	TZS	Shilling	Sh	sw-TZ,en,ar	255	tz			Dodoma	2018-12-03 23:28:50.938813-05	2020-09-13 15:51:24.147508-04	tanzania	6255146	Q924
1512440	Uzbekistan	Uzbekistan	UZ	UZB	27865738	447400	UZS	Som	лв	uz,ru,tg	998	uz	######	^(\\d{6})$	Tashkent	2018-12-03 23:28:50.955201-05	2020-09-13 15:51:30.023748-04	uzbekistan	6255147	Q265
163843	Syria	Syria	SY	SYR	22198110	185180	SYP	Pound	£, ل.س	ar-SY,ku,hy,arc,fr,en	963	sy			Damascus	2018-12-03 23:28:50.893419-05	2020-09-13 15:52:08.04772-04	syria	6255147	Q858
1220409	Tajikistan	Tajikistan	TJ	TJK	7487489	143100	TJS	Somoni	ЅМ	tg,ru	992	tj	######	^(\\d{6})$	Dushanbe	2018-12-03 23:28:50.912542-05	2020-09-13 15:49:47.662921-04	tajikistan	6255147	Q863
2403846	Sierra Leone	Sierra Leone	SL	SLE	5245695	71740	SLL	Leone	Le	en-SL,men,tem	232	sl			Freetown	2018-12-03 23:28:50.872981-05	2020-09-13 15:56:08.817449-04	sierra-leone	6255146	Q1044
1218197	Turkmenistan	Turkmenistan	TM	TKM	4940916	488100	TMT	Manat	m	tk,ru,uz	993	tm	######	^(\\d{6})$	Ashgabat	2018-12-03 23:28:50.920556-05	2020-09-13 15:49:46.970129-04	turkmenistan	6255147	Q874
3164670	Vatican	Vatican	VA	VAT	921	0	EUR	Euro	€	la,it,fr	379	va	#####	^(\\d{5})$	Vatican City	2018-12-03 23:28:50.957954-05	2020-09-13 16:02:56.208701-04	vatican	6255148	Q237
3573591	Trinidad and Tobago	Trinidad and Tobago	TT	TTO	1328019	5128	TTD	Dollar	$	en-TT,hns,fr,es,zh	+1-868	tt			Port of Spain	2018-12-03 23:28:50.931103-05	2020-09-13 16:04:24.372163-04	trinidad-and-tobago	6255149	Q754
690791	Ukraine	Ukraine	UA	UKR	45415596	603700	UAH	Hryvnia	₴	uk,ru-UA,rom,pl,hu	380	ua	#####	^(\\d{5})$	Kyiv	2018-12-03 23:28:50.941683-05	2020-09-13 16:16:51.543885-04	ukraine	6255148	Q212
2434508	Chad	Chad	TD	TCD	10543464	1284000	XAF	Franc	Fr	fr-TD,ar-TD,sre	235	td			N'Djamena	2018-12-03 23:28:50.90115-05	2020-09-13 15:56:18.535983-04	chad	6255146	Q657
2410758	Sao Tome and Principe	Sao Tome and Principe	ST	STP	175808	1001	STD	Dobra	Db	pt-ST	239	st			Sao Tome	2018-12-03 23:28:50.885912-05	2020-09-13 15:56:11.00755-04	sao-tome-and-principe	6255146	Q1039
2363686	Togo	Togo	TG	TGO	6587239	56785	XOF	Franc	Fr	fr-TG,ee,hna,kbp,dag,ha	228	tg			Lome	2018-12-03 23:28:50.907125-05	2020-09-13 15:55:54.050062-04	togo	6255146	Q945
241170	Seychelles	Seychelles	SC	SYC	88340	455	SCR	Rupee	₨	en-SC,fr-SC	248	sc			Victoria	2018-12-03 23:28:50.849519-05	2020-09-13 15:56:11.506326-04	seychelles	6255146	Q1042
226074	Uganda	Uganda	UG	UGA	33398682	236040	UGX	Shilling	Sh	en-UG,lg,sw,ar	256	ug			Kampala	2018-12-03 23:28:50.944508-05	2020-09-13 15:55:35.285366-04	uganda	6255146	Q1036
1562822	Vietnam	Vietnam	VN	VNM	89571130	329560	VND	Dong	₫	vi,en,fr,zh,km	84	vn	######	^(\\d{6})$	Hanoi	2018-12-03 23:28:50.972556-05	2020-09-13 15:51:47.086692-04	vietnam	6255147	Q881
1966436	Timor Leste	Timor Leste	TL	TLS	1154625	15007	USD	Dollar	$	tet,pt-TL,id,en	670	tl			Dili	2018-12-03 23:28:50.918066-05	2020-09-13 15:53:50.294321-04	timor-leste	6255151	Q574
2245662	Senegal	Senegal	SN	SEN	12323252	196190	XOF	Franc	Fr	fr-SN,wo,fuc,mnk	221	sn	#####	^(\\d{5})$	Dakar	2018-12-03 23:28:50.878225-05	2020-09-13 15:55:30.277208-04	senegal	6255146	Q1041
298795	Turkey	Turkey	TR	TUR	77804122	780580	TRY	Lira	₤	tr-TR,ku,diq,az,av	90	tr	#####	^(\\d{5})$	Ankara	2018-12-03 23:28:50.928494-05	2020-09-13 16:00:13.684667-04	turkey	6255147	Q43
3168068	San Marino	San Marino	SM	SMR	31477	61	EUR	Euro	€	it-SM	378	sm	4789#	^(4789\\d)$	San Marino	2018-12-03 23:28:50.87551-05	2020-09-13 16:02:56.868791-04	san-marino	6255148	Q238
3190538	Slovenia	Slovenia	SI	SVN	2007000	20273	EUR	Euro	€	sl,sh	386	si	####	^(?:SI)*(\\d{4})$	Ljubljana	2018-12-03 23:28:50.865569-05	2020-09-13 16:03:03.146657-04	slovenia	6255148	Q215
3370751	Saint Helena	Saint Helena	SH	SHN	7460	410	SHP	Pound	£	en-SH	290	sh	STHL 1ZZ	^(STHL1ZZ)$	Jamestown	2018-12-03 23:28:50.862901-05	2020-09-13 16:03:31.499496-04	saint-helena	6255146	Q192184
3382998	Suriname	Suriname	SR	SUR	492829	163270	SRD	Dollar	$	nl-SR,en,srn,hns,jv	597	sr			Paramaribo	2018-12-03 23:28:50.883168-05	2020-09-13 16:03:33.825861-04	suriname	6255150	Q730
3439705	Uruguay	Uruguay	UY	URY	3477000	176220	UYU	Peso	$	es-UY	598	uy	#####	^(\\d{5})$	Montevideo	2018-12-03 23:28:50.95265-05	2020-09-13 16:03:47.171958-04	uruguay	6255150	Q77
3577815	Saint Vincent and the Grenadines	Saint Vincent and the Grenadines	VC	VCT	104217	389	XCD	Dollar	$	en-VC,fr	+1-784	vc			Kingstown	2018-12-03 23:28:50.960583-05	2020-09-13 16:04:24.977668-04	saint-vincent-and-the-grenadines	6255149	Q757
4032283	Tonga	Tonga	TO	TON	122580	748	TOP	Pa'anga	T$	to,en-TO	676	to			Nuku'alofa	2018-12-03 23:28:50.926027-05	2020-09-13 16:05:39.170496-04	tonga	6255151	Q678
51537	Somalia	Somalia	SO	SOM	10112453	637657	SOS	Shilling	Sh	so-SO,ar-SO,it,en-SO	252	so	@@  #####	^([A-Z]{2}\\d{5})$	Mogadishu	2018-12-03 23:28:50.880709-05	2020-09-13 16:09:12.820861-04	somalia	6255146	Q1045
6252001	United States	United States	US	USA	310232863	9629091	USD	Dollar	$	en-US,es-US,haw,fr	1	us	#####-####	^\\d{5}(-\\d{4})?$	Washington	2018-12-03 23:28:50.950066-05	2020-09-13 16:11:56.093737-04	united-states	6255149	Q30
7609695	Sint Maarten	Sint Maarten	SX	SXM	37429	21	ANG	Guilder	ƒ	nl,en	599	sx			Philipsburg	2018-12-03 23:28:50.890946-05	2020-09-13 16:19:03.079637-04	sint-maarten	6255149	Q26273
7909807	South Sudan	South Sudan	SS	SSD	8260490	644329	SSP	Pound	\N	en	211				Juba	2018-12-03 23:28:50.854881-05	2020-09-13 16:20:11.843527-04	south-sudan	6255146	Q958
878675	Zimbabwe	Zimbabwe	ZW	ZWE	13061000	390580	ZWL	Dollar	$	en-ZW,sn,nr,nd	263	zw			Harare	2018-12-03 23:28:50.992635-05	2020-09-13 16:22:13.661812-04	zimbabwe	6255146	Q954
2635167	United Kingdom	United Kingdom	GB	GBR	62348447	244820	GBP	Pound	£	en-GB,cy-GB,gd	44	uk	@# #@@|@## #@@|@@# #@@|@@## #@@|@#@ #@@|@@#@ #@@|GIR0AA	^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$	London	2018-12-03 23:28:50.528198-05	2020-09-13 15:57:38.949029-04	united-kingdom	6255148	Q145
895949	Zambia	Zambia	ZM	ZMB	13460305	752614	ZMW	Kwacha	\N	en-ZM,bem,loz,lun,lue,ny,toi	260	zm	#####	^(\\d{5})$	Lusaka	2018-12-03 23:28:50.99012-05	2020-09-13 16:22:24.08029-04	zambia	6255146	Q953
953987	South Africa	South Africa	ZA	ZAF	49000000	1219912	ZAR	Rand	R	zu,xh,af,nso,en-ZA,tn,st,ts,ss,ve,nr	27	za	####	^(\\d{4})$	Pretoria	2018-12-03 23:28:50.987571-05	2020-09-13 16:22:47.335538-04	south-africa	6255146	Q258
294640	Israel	Israel	IL	ISR	7353985	20770	ILS	Shekel	₪	he,ar-IL,en-IL,	972	il	#####	^(\\d{5})$	Jerusalem	2018-12-03 23:28:50.596269-05	2020-09-13 15:59:51.733558-04	israel	6255147	Q801
2963597	Ireland	Ireland	IE	IRL	4622917	70280	EUR	Euro	€	en-IE,ga-IE	353	ie	@@@ @@@@	^[A-Z]\\d{2}$|^[A-Z]{3}[A-Z]{4}$	Dublin	2018-12-03 23:28:50.593717-05	2020-09-13 15:59:57.877334-04	ireland	6255148	Q27
3042142	Jersey	Jersey	JE	JEY	90812	116	GBP	Pound	£	en,fr,nrf	+44-1534	je	@# #@@|@## #@@|@@# #@@|@@## #@@|@#@ #@@|@@#@ #@@|GIR0AA	^((?:(?:[A-PR-UWYZ][A-HK-Y]\\d[ABEHMNPRV-Y0-9]|[A-PR-UWYZ]\\d[A-HJKPS-UW0-9])\\s\\d[ABD-HJLNP-UW-Z]{2})|GIR\\s?0AA)$	Saint Helier	2018-12-03 23:28:50.617068-05	2020-09-13 16:00:45.021865-04	jersey	6255148	Q785
3042362	Guernsey	Guernsey	GG	GGY	65228	78	GBP	Pound	£	en,nrf	+44-1481	gg	@# #@@|@## #@@|@@# #@@|@@## #@@|@#@ #@@|@@#@ #@@|GIR0AA	^((?:(?:[A-PR-UWYZ][A-HK-Y]\\d[ABEHMNPRV-Y0-9]|[A-PR-UWYZ]\\d[A-HJKPS-UW0-9])\\s\\d[ABD-HJLNP-UW-Z]{2})|GIR\\s?0AA)$	St Peter Port	2018-12-03 23:28:50.53826-05	2020-09-13 16:00:45.095285-04	guernsey	6255148	Q25230
3057568	Slovakia	Slovakia	SK	SVK	5455000	48845	EUR	Euro	€	sk,hu	421	sk	### ##	^\\d{3}\\s?\\d{2}$	Bratislava	2018-12-03 23:28:50.870464-05	2020-09-13 16:00:56.000129-04	slovakia	6255148	Q214
337996	Ethiopia	Ethiopia	ET	ETH	88013491	1127127	ETB	Birr	Br	am,en-ET,om-ET,ti-ET,so-ET,sid	251	et	####	^(\\d{4})$	Addis Ababa	2018-12-03 23:28:50.50728-05	2020-09-13 16:03:33.390107-04	ethiopia	6255146	Q115
1024031	Mayotte	Mayotte	YT	MYT	159042	374	EUR	Euro	€	fr-YT	262	yt	#####	^(\\d{5})$	Mamoudzou	2018-12-03 23:28:50.98477-05	2020-09-13 15:47:58.044728-04	mayotte	6255146	Q17063
3576396	Antigua and Barbuda	Antigua and Barbuda	AG	ATG	86754	443	XCD	Dollar	$	en-AG	+1-268	ag			St. John's	2018-12-03 23:28:50.335586-05	2020-09-13 16:04:24.762114-04	antigua-and-barbuda	6255149	Q781
1149361	Afghanistan	Afghanistan	AF	AFG	29121286	647500	AFN	Afghani	؋	fa-AF,ps,uz-AF,tk	93	af			Kabul	2018-12-03 23:28:50.33317-05	2020-09-13 15:49:18.721272-04	afghanistan	6255147	Q889
1733045	Malaysia	Malaysia	MY	MYS	28274729	329750	MYR	Ringgit	RM	ms-MY,en,zh,ta,te,ml,pa,th	60	my	#####	^(\\d{5})$	Kuala Lumpur	2018-12-03 23:28:50.75062-05	2020-09-13 15:52:44.623739-04	malaysia	6255147	Q833
1814991	China	China	CN	CHN	1330044000	9596960	CNY	Yuan Renminbi	¥	zh-CN,yue,wuu,dta,ug,za	86	cn	######	^(\\d{6})$	Beijing	2018-12-03 23:28:50.450736-05	2020-09-13 15:53:10.437704-04	china	6255147	Q148
192950	Kenya	Kenya	KE	KEN	40046566	582650	KES	Shilling	Sh	en-KE,sw-KE	254	ke	#####	^(\\d{5})$	Nairobi	2018-12-03 23:28:50.628545-05	2020-09-13 15:53:43.461258-04	kenya	6255146	Q114
203312	Democratic Republic of the Congo	Democratic Republic of the Congo	CD	COD	70916439	2345410	CDF	Franc	Fr	fr-CD,ln,ktu,kg,sw,lua	243	cd			Kinshasa	2018-12-03 23:28:50.429254-05	2020-09-13 15:54:10.201184-04	democratic-republic-of-the-congo	6255146	Q974
3576916	Turks and Caicos Islands	Turks and Caicos Islands	TC	TCA	20556	430	USD	Dollar	$	en-TC	+1-649	tc	TKCA 1ZZ	^(TKCA 1ZZ)$	Cockburn Town	2018-12-03 23:28:50.898591-05	2020-09-13 16:04:24.850725-04	turks-and-caicos-islands	6255149	Q18221
4030656	French Polynesia	French Polynesia	PF	PYF	270485	4167	XPF	Franc	Fr	fr-PF,ty	689	pf	#####	^((97|98)7\\d{2})$	Papeete	2018-12-03 23:28:50.795584-05	2020-09-13 16:05:38.826721-04	french-polynesia	6255151	Q30971
4034749	Wallis and Futuna	Wallis and Futuna	WF	WLF	16025	274	XPF	Franc	Fr	wls,fud,fr-WF	681	wf	#####	^(986\\d{2})$	Mata Utu	2018-12-03 23:28:50.977473-05	2020-09-13 16:05:39.739056-04	wallis-and-futuna	6255151	Q35555
4034894	Samoa	Samoa	WS	WSM	192001	2944	WST	Tala	T	sm,en-WS	685	ws			Apia	2018-12-03 23:28:50.98001-05	2020-09-13 16:05:39.751565-04	samoa	6255151	Q683
2134431	Vanuatu	Vanuatu	VU	VUT	221552	12200	VUV	Vatu	Vt	bi,en-VU,fr-VU	678	vu			Port Vila	2018-12-03 23:28:50.974984-05	2020-09-13 15:54:42.454142-04	vanuatu	6255151	Q686
2328926	Nigeria	Nigeria	NG	NGA	154000000	923768	NGN	Naira	₦	en-NG,ha,yo,ig,ff	234	ng	######	^(\\d{6})$	Abuja	2018-12-03 23:28:50.766804-05	2020-09-13 15:55:48.113385-04	nigeria	6255146	Q1033
2464461	Tunisia	Tunisia	TN	TUN	10589025	163610	TND	Dinar	د.ت	ar-TN,fr	216	tn	####	^(\\d{4})$	Tunis	2018-12-03 23:28:50.923102-05	2020-09-13 15:56:25.794651-04	tunisia	6255146	Q948
69543	Yemen	Yemen	YE	YEM	23495361	527970	YER	Rial	﷼	ar-YE	967	ye			Sanaa	2018-12-03 23:28:50.982452-05	2020-09-13 16:17:03.08983-04	yemen	6255147	Q805
\.


--
-- Name: locus_country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: argus
--

SELECT pg_catalog.setval('public.locus_country_id_seq', 7909808, false);


--
-- PostgreSQL database dump complete
--

