--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4
-- Dumped by pg_dump version 12.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: locus_continent; Type: TABLE DATA; Schema: public; Owner: argus
--

COPY public.locus_continent (id, name, code, slug, wikidata) FROM stdin;
6255146	Africa	AF	Africa	\N
6255147	Asia	AS	Asia	\N
6255148	Europe	EU	Europe	\N
6255149	North America	NA	North America	\N
6255150	South America	SA	South America	\N
6255151	Oceania	OC	Oceania	\N
6255152	Antarctica	AN	Antarctica	\N
\.


--
-- Name: locus_continent_id_seq; Type: SEQUENCE SET; Schema: public; Owner: argus
--

SELECT pg_catalog.setval('public.locus_continent_id_seq', 6255153, false);


--
-- PostgreSQL database dump complete
--

